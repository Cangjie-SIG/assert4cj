/*
 * Copyright © 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package assert4cj_ut.predicate

import std.unittest.testmacro.*

/**
 * CollectionPredicateTest class.
 *
 * @author changjun
 * @version 0.1.0
 * @since 2024/09/05
 */
@Test
public class CollectionPredicateTest {

    // ----------------------------------------------------------------

    @TestCase
    public func testIsEmpty_None() {
        let x1 = Option<Collection<Int64>>.None
        let x2 = Option<Collection<String>>.None

        Assertions.assertTrue(CollectionPredicate.isEmpty(x1))
        Assertions.assertTrue(CollectionPredicate.isEmpty(x2))
    }

    @TestCase
    public func testIsEmpty_Some() {
        let x1 = Option<Collection<Int64>>.None
        let c1 = ArrayList<Int64>([1, 2, 3, 10086])
        let y1 = Option<Collection<Int64>>.Some(c1)

        let x2 = Option<Collection<String>>.None
        let c2 = ArrayList<String>(["Hello, World"])
        let y2 = Option<Collection<String>>.Some(c2)

        Assertions.assertTrue(CollectionPredicate.isEmpty(x1))
        Assertions.assertTrue(CollectionPredicate.isEmpty(x2))

        Assertions.assertFalse(CollectionPredicate.isEmpty(y1))
        Assertions.assertFalse(CollectionPredicate.isEmpty(y2))
    }

    @TestCase
    public func testIsNotEmpty_None() {
        let x1 = Option<Collection<Int64>>.None
        let x2 = Option<Collection<String>>.None

        Assertions.assertFalse(CollectionPredicate.isNotEmpty(x1))
        Assertions.assertFalse(CollectionPredicate.isNotEmpty(x2))
    }

    @TestCase
    public func testIsNotEmpty_Some() {
        let x1 = Option<Collection<Int64>>.None
        let c1 = ArrayList<Int64>([1, 2, 3, 10086])
        let y1 = Option<Collection<Int64>>.Some(c1)

        let x2 = Option<Collection<String>>.None
        let c2 = ArrayList<String>(["Hello, World"])
        let y2 = Option<Collection<String>>.Some(c2)

        Assertions.assertFalse(CollectionPredicate.isNotEmpty(x1))
        Assertions.assertFalse(CollectionPredicate.isNotEmpty(x2))

        Assertions.assertTrue(CollectionPredicate.isNotEmpty(y1))
        Assertions.assertTrue(CollectionPredicate.isNotEmpty(y2))
    }

    // ----------------------------------------------------------------

    @TestCase
    public func testIsEmpty() {
        let x1 = ArrayList<Int64>([1, 2, 3, 10086])
        let y1 = ArrayList<Int64>()

        let x2 = LinkedList<Int64>([1, 2, 3, 10086])
        let y2 = LinkedList<Int64>()

        let x3 = HashSet<Int64>([1, 2, 3, 10086])
        let y3 = HashSet<Int64>()

        Assertions.assertFalse(CollectionPredicate.isEmpty(x1))
        Assertions.assertFalse(CollectionPredicate.isEmpty(x2))
        Assertions.assertFalse(CollectionPredicate.isEmpty(x3))

        Assertions.assertTrue(CollectionPredicate.isEmpty(y1))
        Assertions.assertTrue(CollectionPredicate.isEmpty(y2))
        Assertions.assertTrue(CollectionPredicate.isEmpty(y3))
    }

    @TestCase
    public func testIsNotEmpty() {
        let x1 = ArrayList<Int64>([1, 2, 3, 10086])
        let y1 = ArrayList<Int64>()

        let x2 = LinkedList<Int64>([1, 2, 3, 10086])
        let y2 = LinkedList<Int64>()

        let x3 = HashSet<Int64>([1, 2, 3, 10086])
        let y3 = HashSet<Int64>()

        Assertions.assertTrue(CollectionPredicate.isNotEmpty(x1))
        Assertions.assertTrue(CollectionPredicate.isNotEmpty(x2))
        Assertions.assertTrue(CollectionPredicate.isNotEmpty(x3))

        Assertions.assertFalse(CollectionPredicate.isNotEmpty(y1))
        Assertions.assertFalse(CollectionPredicate.isNotEmpty(y2))
        Assertions.assertFalse(CollectionPredicate.isNotEmpty(y3))
    }

    // ----------------------------------------------------------------

    @TestCase
    public func testArrayListIsContains() {
        let x1 = ArrayList<Int64>([1, 2, 3, 10086])

        Assertions.assertTrue(CollectionPredicate.isContains(x1, 10086))
        Assertions.assertFalse(CollectionPredicate.isContains(x1, 4))
    }

    @TestCase
    public func testArrayListIsNotContains() {
        let x1 = ArrayList<Int64>([1, 2, 3, 10086])

        Assertions.assertFalse(CollectionPredicate.isNotContains(x1, 10086))
        Assertions.assertTrue(CollectionPredicate.isNotContains(x1, 4))
    }

    @TestCase
    public func testSetIsContains() {
        let x1 = HashSet<Int64>([1, 2, 3, 10086])

        Assertions.assertTrue(CollectionPredicate.isContains(x1, 10086))
        Assertions.assertFalse(CollectionPredicate.isContains(x1, 4))
    }

    @TestCase
    public func testSetIsNotContains() {
        let x1 = HashSet<Int64>([1, 2, 3, 10086])

        Assertions.assertFalse(CollectionPredicate.isNotContains(x1, 10086))
        Assertions.assertTrue(CollectionPredicate.isNotContains(x1, 4))
    }
}
