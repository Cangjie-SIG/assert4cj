# `Feature API`

设计了两个断言类 `Assertions` 和 `Expectations`，分别对应宏  `@Assert` 和 `@Expect` 。

- `Assertions`
  - `@Assert(...)`
- `Expectations`
  - `@Expect(...)`



## 1.`Assertions`

导入(包): `import assert4cj.unittest.*`

### 1.1.`assertEquals`

- `==` | 相等

```cangjie
@TestCase
public func testAssertEquals() {
    Assertions.assertEquals(true, true)
    Assertions.assertEquals(false, false)
    Assertions.assertEquals(1, 1)
    Assertions.assertEquals("Hello", "Hello")

    @AssertThrows[AssertException](Assertions.assertEquals("Hello", "World", "Hello != World"))
    @AssertThrows[AssertException](Assertions.assertEquals("Hello", "World"))
    @AssertThrows[AssertException](Assertions.assertEquals(true, false))
}
```

### 1.2.`assertTrue`

- `true` | 真

```cangjie
@TestCase
public func testAssertTrue() {
    Assertions.assertTrue(true)
    Assertions.assertTrue(1 == 1)
    Assertions.assertTrue("Hello" == "Hello")

    @AssertThrows[AssertException](Assertions.assertTrue("Hello" == "World", "Hello != World"))
    @AssertThrows[AssertException](Assertions.assertTrue("Hello" == "World"))
    @AssertThrows[AssertException](Assertions.assertTrue(false))
}
```

### 1.3.`assertFalse`

- `false` | 假

```cangjie
@TestCase
public func testAssertFalse() {
    Assertions.assertFalse(false)
    Assertions.assertFalse(1 == 2)
    Assertions.assertFalse("Hello" == "World")

    @AssertThrows[AssertException](Assertions.assertFalse("Hello" == "Hello", "Hello != World"))
    @AssertThrows[AssertException](Assertions.assertFalse("Hello" == "Hello"))
    @AssertThrows[AssertException](Assertions.assertFalse(true))
}
```

### 1.4.`assertGt`

- `>` | 大于

```cangjie
@TestCase
public func testAssertGt() {
    Assertions.assertGt(2, 1)

    @AssertThrows[AssertException](Assertions.assertGt(1, 2, "1 < 2"))
    @AssertThrows[AssertException](Assertions.assertGt(2, 2, "2 == 2"))
}
```

### 1.5.`assertGte`

- `>=` 大于等于

```cangjie
@TestCase
public func testAssertGte() {
    Assertions.assertGte(2, 1)
    Assertions.assertGte(2, 2)

    @AssertThrows[AssertException](Assertions.assertGte(1, 2, "1 < 2"))
}
```

### 1.6.`assertLt`

- `<` | 小于

```cangjie
@TestCase
public func testAssertLt() {
    Assertions.assertLt(1, 2)

    @AssertThrows[AssertException](Assertions.assertLt(2, 1, "2 > 1"))
    @AssertThrows[AssertException](Assertions.assertLt(2, 2, "2 == 2"))
}
```

### 1.7.`assertLte`

- `<=` | 小于等于

```cangjie
@TestCase
public func testAssertLte() {
    Assertions.assertLte(1, 2)
    Assertions.assertLte(2, 2)

    @AssertThrows[AssertException](Assertions.assertLte(2, 1, "2 > 1"))
}
```

### 1.8.`assertNone`

- `Option<T>.None` | `None`

```cangjie
@TestCase
public func testAssertNone() {
    let x: Option<Int64> = Option<Int64>.None
    let y: Option<Int64> = Option<Int64>.Some(10086)

    Assertions.assertNone(x)

    @AssertThrows[AssertException](Assertions.assertNone(y, "y: Some(10086)"))
}
```

### 1.9.`assertSome`

- `Option<T>.Some(...)` | `Some`

```cangjie
@TestCase
public func testAssertSome() {
    let x: Option<Int64> = Option<Int64>.Some(10086)
    let y: Option<Int64> = Option<Int64>.None

    Assertions.assertSome(x)

    @AssertThrows[AssertException](Assertions.assertSome(y, "y: Option<Int64>.None"))
}
```

### 1.10.`assertEmpty`

- `size == 0` | 列表为空

- `ArrayList` | 数组实现的列表

  - ```cangjie
    @TestCase
    public func testArrayListAssertEmpty() {
        let x = ArrayList<Int64>()
        let y = ArrayList<Int64>([1, 2, 3, 10086])
    
        Assertions.assertEmpty(x)
    
        @AssertThrows[AssertException](Assertions.assertEmpty(y, "y: Not empty"))
    }
    ```

- `LinkedList` | 链表实现的列表

  - ```cangjie
    @TestCase
    public func testLinkedListAssertEmpty() {
        let x = LinkedList<Int64>()
        let y = LinkedList<Int64>([1, 2, 3, 10086])
    
        Assertions.assertEmpty(x)
    
        @AssertThrows[AssertException](Assertions.assertEmpty(y, "y: Not empty"))
    }
    ```

- `HashSet` | `HashSet`

  - ```cangjie
    @TestCase
    public func testSetAssertEmpty() {
        let x = HashSet<Int64>()
        let y = HashSet<Int64>([1, 2, 3, 10086])
    
        Assertions.assertEmpty(x)
    
        @AssertThrows[AssertException](Assertions.assertEmpty(y, "y: Not empty"))
    }
    ```

- `HashMap` | `HashMap`

  - ```cangjie
    @TestCase
    public func testHashMapAssertEmpty() {
        let x1 = HashMap<String, String>()
        let y1 = HashMap<String, String>([("hello", "cangjie")])
    
        let x2 = HashMap<String, Int64>()
        let y2 = HashMap<String, Int64>([("hello", 10086)])
    
        Assertions.assertEmpty(x1)
        Assertions.assertEmpty(x2)
    
        @AssertThrows[AssertException](Assertions.assertEmpty(y1, "y1: Not empty"))
        @AssertThrows[AssertException](Assertions.assertEmpty(y2, "y2: Not empty"))
    }
    ```


### 1.11.`assertNotEmpty`

- `size > 0` | 列表不为空

- `ArrayList` | 数组实现的列表

  - ```cangjie
    @TestCase
    public func testArrayListAssertNotEmpty() {
        let x = ArrayList<Int64>([1, 2, 3, 10086])
        let y = ArrayList<Int64>()
    
        Assertions.assertNotEmpty(x)
    
        @AssertThrows[AssertException](Assertions.assertNotEmpty(y, "y: empty"))
    }
    ```

- `LinkedList` | 链表实现的列表

  - ```cangjie
    @TestCase
    public func testLinkedListAssertNotEmpty() {
        let x = LinkedList<Int64>([1, 2, 3, 10086])
        let y = LinkedList<Int64>()
    
        Assertions.assertNotEmpty(x)
    
        @AssertThrows[AssertException](Assertions.assertNotEmpty(y, "y: empty"))
    }
    ```

- `HashSet` | `HashSet`

  - ```cangjie
    @TestCase
    public func testSetAssertNotEmpty() {
        let x = HashSet<Int64>([1, 2, 3, 10086])
        let y = HashSet<Int64>()
    
        Assertions.assertNotEmpty(x)
    
        @AssertThrows[AssertException](Assertions.assertNotEmpty(y, "y: empty"))
    }
    ```

- `HashMap` | `HashMap`

  - ```cangjie
    @TestCase
    public func testHashMapAssertNotEmpty() {
        let x1 = HashMap<String, String>([("hello", "cangjie")])
        let y1 = HashMap<String, String>()
    
        let x2 = HashMap<String, Int64>([("hello", 10086)])
        let y2 = HashMap<String, Int64>()
    
        Assertions.assertNotEmpty(x1)
        Assertions.assertNotEmpty(x2)
    
        @AssertThrows[AssertException](Assertions.assertNotEmpty(y1, "y1: empty"))
        @AssertThrows[AssertException](Assertions.assertNotEmpty(y2, "y2: empty"))
    }
    ```


### 1.12.`assertContains`

- `Contains` | 包含关系

- `ArrayList` | 数组实现的列表

  - ```cangjie
    @TestCase
    public func testArrayListAssertContains() {
        let elements = ArrayList<Int64>([1, 2, 3, 10086])
    
        Assertions.assertContains(elements, 10086)
    
        @AssertThrows[AssertException](Assertions.assertContains(elements, 4, "x: Not contains 4"))
    }
    ```

- `HashSet` | `HashSet`

  - ```cangjie
    @TestCase
    public func testHashSetAssertContains() {
        let elements = HashSet<Int64>([1, 2, 3, 3, 10086])
    
        Assertions.assertContains(elements, 10086)
    
        @AssertThrows[AssertException](Assertions.assertContains(elements, 4, "x: Not contains 4"))
    }
    ```

### 1.13.`assertNotContains`

- `Not contains` | 不包含

- `ArrayList` | 数组实现的列表

  - ```cangjie
    @TestCase
    public func testArrayListAssertNotContains() {
        let elements = ArrayList<Int64>([1, 2, 3, 10086])
    
        Assertions.assertNotContains(elements, 4)
    
        @AssertThrows[AssertException](Assertions.assertNotContains(elements, 10086, "x: contains 10086"))
    }
    ```

- `HashSet` | `HashSet`

  - ```cangjie
    @TestCase
    public func testHashSetAssertNotContains() {
        let elements = HashSet<Int64>([1, 2, 3, 3, 10086])
    
        Assertions.assertNotContains(elements, 4)
    
        @AssertThrows[AssertException](Assertions.assertNotContains(elements, 10086, "x: contains 10086"))
    }
    ```



## 2.`Expectations`

### 2.1.`expectEquals`

- `==` | 相等

```cangjie
@TestCase
public func testExpectEquals() {
    Expectations.expectEquals(true, true)
    Expectations.expectEquals(false, false)
    Expectations.expectEquals(1, 1)
    Expectations.expectEquals("Hello", "Hello")

    // FIXME: 我当前还不太清楚怎么断言,断言会发生错误该怎么写
    // 断言-断言失败场景:
    // Expectations.expectEquals("Hello", "World")
    // Expectations.expectEquals(true, false)
    // ...
}
```

### 2.2.`expectTrue`

- `true` | 真

```cangjie
@TestCase
public func testExpectTrue() {
    Expectations.expectTrue(true)
    Expectations.expectTrue(1 == 1)
    Expectations.expectTrue("Hello" == "Hello")

    // FIXME: 我当前还不太清楚怎么断言,断言会发生错误该怎么写
    // 断言-断言失败场景:
    // Expectations.expectTrue(false)
    // Expectations.expectTrue(1 != 1)
    // Expectations.expectTrue("Hello" != "Hello")
    // ...
}
```

### 2.3.`expectFalse`

- `false` | 假

```
@TestCase
public func testExpectFalse() {
    Expectations.expectFalse(false)
    Expectations.expectFalse(1 != 1)
    Expectations.expectFalse("Hello" != "Hello")

    // FIXME: 我当前还不太清楚怎么断言,断言会发生错误该怎么写
    // 断言-断言失败场景:
}
```

### 2.4.`expectGt`

- `>` | 大于

```cangjie
@TestCase
public func testExpectGt() {
    Expectations.expectGt(2, 1)

    // FIXME: 我当前还不太清楚怎么断言,断言会发生错误该怎么写
    // 断言-断言失败场景:
}
```

### 2.5.`expectGte`

- `>=` | 大于等于

```cangjie
@TestCase
public func testExpectGte() {
    Expectations.expectGte(2, 1)
    Expectations.expectGte(2, 2)

    // FIXME: 我当前还不太清楚怎么断言,断言会发生错误该怎么写
    // 断言-断言失败场景:
}
```

### 2.6.`expectLt`

- `<` | 小于

```cangjie
@TestCase
public func testExpectLt() {
    Expectations.expectLt(1, 2)

    // FIXME: 我当前还不太清楚怎么断言,断言会发生错误该怎么写
    // 断言-断言失败场景:
}
```

### 2.7.`expectLte`

- `<=` | 小于等于

```cangjie
@TestCase
public func testExpectLte() {
    Expectations.expectLte(1, 2)
    Expectations.expectLte(2, 2)

    // FIXME: 我当前还不太清楚怎么断言,断言会发生错误该怎么写
    // 断言-断言失败场景:
}
```

### 2.8.`expectNone`

- `Option<T>.None` | `None`

```cangjie
@TestCase
public func testExpectNone() {
    Expectations.expectNone(Option<Int64>.None)

    // FIXME: 我当前还不太清楚怎么断言,断言会发生错误该怎么写
    // 断言-断言失败场景:
}
```

### 2.9.`expectSome`

- `Option<T>.Some(...) `| `Some`

```cangjie
@TestCase
public func testExpectSome() {
    Expectations.expectSome(Option<Int64>.Some(10086))

    // FIXME: 我当前还不太清楚怎么断言,断言会发生错误该怎么写
    // 断言-断言失败场景:
}
```

### 2.10.`expectEmpty`

- `size == 0` | 列表为空

- `Collection` | `<Collection>` 类型

```cangjie
@TestCase
public func testExpectEmpty() {
    let haystack = ArrayList<Int64>()
    Expectations.expectEmpty(haystack)

    // FIXME: 我当前还不太清楚怎么断言,断言会发生错误该怎么写
    // 断言-断言失败场景:
}
```

### 2.11.`expectNotEmpty`

- `size > 0` | 列表不为空

- `Collection` | `<Collection>` 类型

```cangjie
@TestCase
public func testExpectNotEmpty() {
    let haystack = ArrayList<Int64>([1, 2, 3, 10086])
    Expectations.expectNotEmpty(haystack)

    // FIXME: 我当前还不太清楚怎么断言,断言会发生错误该怎么写
    // 断言-断言失败场景:
}
```

### 2.12.`expectContains`

- `Contains` | 包含

- `ArrayList` | 数组实现的列表

  - ```cangjie
    @TestCase
    public func testArrayListExpectContains() {
        let haystack = ArrayList<Int64>([1, 2, 3, 10086])
        Expectations.expectContains(haystack, 10086)
    
        // FIXME: 我当前还不太清楚怎么断言,断言会发生错误该怎么写
        // 断言-断言失败场景:
    }
    ```
    

- `HashSet` | `HashSet`

  - ```cangjie
    @TestCase
    public func testHashSetExpectContains() {
        let haystack = HashSet<Int64>([1, 2, 3, 10086])
        Expectations.expectContains(haystack, 10086)
    
        // FIXME: 我当前还不太清楚怎么断言,断言会发生错误该怎么写
        // 断言-断言失败场景:
    }
    ```

### 2.13.`expectNotContains`

- `Not contains` | 不包含

- `ArrayList` | 数组实现的列表

  - ```cangjie
    @TestCase
    public func testArrayListExpectNotContains() {
        let haystack = ArrayList<Int64>([1, 2, 3, 10086])
        Expectations.expectNotContains(haystack, 4)
    
        // FIXME: 我当前还不太清楚怎么断言,断言会发生错误该怎么写
        // 断言-断言失败场景:
    }
    ```
    

- `HashSet` | `HashSet`

  - ```cangjie
    @TestCase
    public func testHashSetExpectNotContains() {
        let haystack = HashSet<Int64>([1, 2, 3, 10086])
        Expectations.expectNotContains(haystack, 4)
    
        // FIXME: 我当前还不太清楚怎么断言,断言会发生错误该怎么写
        // 断言-断言失败场景:
    }
    ```
    



## 3.`Predicate`

### 3.1.`ArrayPredicate`

- `isEmpty`
- `isNotEmpty`
- `isContains`
- `isNotContains`

### 3.2.`CollectionPredicate`

- `isEmpty`
- `isNotEmpty`
- `isContains`
- `isNotContains`

### 3.3.`HashMapPredicate`

- `isEmpty`
- `isNotEmpty`

### 3.4.`StringPredicate`

- `isEmpty`
- `isNotEmpty`
- `isStartsWith`
- `isNotStartsWith`
- `isEndsWith`
- `isNotEndsWith`
- `isEquals`
- `isNotEquals`
- `isContains`
- `isNotContains`