<div align="center">
<h1>assert4cj</h1>
</div>

<p align="center">
<img alt="" src="https://img.shields.io/badge/release-v0.1.0-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjc-v0.54.3-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjcov-0.0%25-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/state-孵化-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/domain-HOS/Cloud-brightgreen" style="display: inline-block;" />
</p>

## <img alt="" src="./doc/readme-image/readme-icon-introduction.png" style="display: inline-block;" width=3%/> 1 介绍

`assert4cj` 是一个基于函数断言的单元测试库, 受到 [junit-jupiter-api](https://github.com/junit-team/junit5) 启发。

### 1.1 项目特性

基于函数实现单元测试的多场景断言。主要解决现有宏 `@Assert` 和 `@Expect` 在断言时参数类型较少的问题。

### 1.2 项目计划

适配 `@Assert` - `Assertions`

适配 `@Expect` - `Expectations`

## <img alt="" src="./doc/readme-image/readme-icon-framework.png" style="display: inline-block;" width=3%/> 2 架构

### 2.1 项目结构

```shell
.
├── README.md
├── LICENSE
├── CHANGELOG
├── cjpm.toml
├── codequality
    └── HEADER
├── doc
    └── readme-image
    └── feature_api.md
|
└── src
    └── main
        └── cangjie
            └── predicate
                ├── array_predicate.cj
                ├── assert_predicate.cj
                ├── collection_predicate.cj
                ├── hash_map_predicate.cj
                ├── import.cj
                ├── predicate.cj
                ├── string_predicate.cj
            └── unittest
                ├── assert.cj
                ├── expect.cj
                ├── import.cj
            ├── import.cj
            ├── lib.cj
            ├── package.cj
|
└── test
└── HLT
└── LLT
└── UT
    └── src
        └── test
            └── cangjie
                └── unittest
                    ├── assert_test.cj
                    ├── expect_test.cj
                    ├── import.cj
                ├── import.cj
                ├── lib.cj
                ├── package.cj
```

### 2.2 接口说明

#### 2.2.1.`Assertions`

- `assertEquals`
- `assertTrue`
- `assertFalse`
- `assertGt`
- `assertGte`
- `assertLt`
- `assertLte`
- `assertNone`
- `assertSome`
- `assertEmpty`
- `assertNotEmpty`
- `assertContains`
- `assertNotContains`

#### 2.2.2.`Expectations`

- `expectEquals`
- `expectTrue`
- `expectFalse`
- `expectGt`
- `expectGte`
- `expectLt`
- `expectLte`
- `expectNone`
- `expectSome`
- `expectEmpty`
- `expectNotEmpty`
- `expectContains`
- `expectNotContains`

#### 2.2.3.`Predicate`

- `ArrayPredicate`
- `CollectionPredicate`
- `HashMapPredicate`
- `StringPredicate`

## <img alt="" src="./doc/readme-image/readme-icon-compile.png" style="display: inline-block;" width=3%/> 3 使用说明

将如下配置添加到您项目的配置文件 `cjpm.toml`:

```toml
# 通常 test 即可
[dependencies]
  assert4cj = { git = "https://gitcode.com/Cangjie-SIG/assert4cj.git", branch = "master", version = "0.1.0" }
  
[test-dependencies]
  assert4cj = { git = "https://gitcode.com/Cangjie-SIG/assert4cj.git", branch = "master", version = "0.1.0" }
```

### 3.1 编译构建（Win/Linux/Mac）

无需构建,直接引入依赖即可使用。

### 3.2 功能示例

详见 [feature_api](./doc/feature_api.md)

```cj
package your.package

// 1.导入其他包 ...
// import ...
// 2.导入单元测试宏支持
import std.unittest.testmacro.*
// 3.导入 assert4cj 断言支持
import assert4cj.unittest.*

/**
 * AssertionsTest class.
 *
 * @author changjun
 * @version 0.1.0
 * @since 2024/08/04
 */
@Test
public class AssertionsTest {

    // ----------------------------------------------------------------

    @TestCase
    public func testAssertEquals() {
        Assertions.assertEquals(true, true)
        Assertions.assertEquals(false, false)
        Assertions.assertEquals(1, 1)
        Assertions.assertEquals("Hello", "Hello")

        @AssertThrows[AssertException](Assertions.assertEquals("Hello", "World", "Hello != World"))
        @AssertThrows[AssertException](Assertions.assertEquals("Hello", "World"))
        @AssertThrows[AssertException](Assertions.assertEquals(true, false))
    }
}    
```

### 3.3.如何源码测试 | 调试

#### 3.3.1.下载代码

```shell
# 1.HTTPS
$ git clone https://gitcode.com/Cangjie-SIG/assert4cj.git
# 2.SSH
$ git clone git@gitcode.com:Cangjie-SIG/assert4cj.git
```

#### 3.3.2.编辑器打开 | 命令行工具

```shell
# 1.进入项目根目录
$ cd assert4cj
$ pwd
-> /your/path/assert4cj

# 2.进入单元测试目录
$ cd ./test/UT # 期望未来仓颉项目可以直接在根目录执行 `$ cjpm test`

# 3.执行测试
# 3.1.IDE 直接运行测试用例
# 3.2.
$ cjpm test
->
...
Summary: TOTAL: 66
    PASSED: 66, SKIPPED: 0, ERROR: 0
    FAILED: 0
...
```

#### 3.3.3.本库的测试用例说明

##### 3.3.3.1.`@Assert`

```cj
// 对于 @Assert 类的断言 -> 可以测试断言成功和断言失败的场景
```

##### 3.3.3.2.`@Expect`

```cj
// 对于 @Expect 类的断言 -> 由于有 "延迟效应", 所以目前只测试断言成功的场景,断言失败的场景我还不知道该怎么写
```


## <img alt="" src="./doc/readme-image/readme-icon-contribute.png" style="display: inline-block;" width=3%/> 4 参与贡献

本项目由 changjun 实现并维护。技术支持和意见反馈请提Issue。

本项目基于 Apache License 2.0，欢迎给我们提交PR，欢迎参与任何形式的贡献。

本项目commiter：[@changjun](https://gitcode.com/changjun)
